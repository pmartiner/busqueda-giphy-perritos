# Buscador de GIFs de perritos 🐕‍🦺
The goal of this project is to search for dog GIFs based on Giphy's [developer APIs](https://developers.giphy.com/docs/api/#quick-start-guide). The website main feature is the *search bar*. The search bar prepends the word `perrito` so that each search is dog related (since I couldn't find a way to filter the search results by category). The *Acerca de* page shows a little bit about the application and why I did it. Everything was developed with `typescript@4.4.4`, `tailwind@2.2.17` and `vue@3.0.0`. This was my first app developed with `vue` and `tailwind` 🥳🎊. You can find the app at [https://busqueda-giphy-perritos.vercel.app/#/](https://busqueda-giphy-perritos.vercel.app/#/), which was deployed with `Vercel`.

## Requirements
- `yarn@1.2.x`
- `node@lts`

## Getting Started

First install the dependencies:

```bash
yarn install
```

Then, run the development server:

```bash
yarn serve
```

Open [http://localhost:8080](http://localhost:8080) with your browser to see the result.

You can start editing the page by modifying `src/views/Home.vue`. The page auto-updates as you edit the file.

## Production Mode

If you want to run the production mode, first you have to build the project:

```bash
yarn build
```

## Linting

Then you can fix the linting issues by running:
```bash
yarn lint
```